# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function


from django.http import HttpRequest, HttpResponse
from django.contrib.auth.models import User, AnonymousUser
from django.core.exceptions import PermissionDenied
from django.utils.http import urlencode
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.test import override_settings, RequestFactory
from django.conf import settings

from .test_utils import URLGateKeeperTestCase
from . import add_token, middleware as middle


@override_settings(
    URL_GATEKEEPER_LOGIN_EXEMPT_URLS=(r"/public/.*$",),
    LOGIN_URL="/accounts/login/",
    LOGOUT_URL="/accounts/logout/",
)
class TestLoginRequired(URLGateKeeperTestCase):
    def setUp(self):
        self.middle = middle.LoginRequiredMiddleware()
        self.request = HttpRequest()
        self.request_factory = RequestFactory()

    def test_login_required(self):
        path = "/private/foo"
        expected_redirect = "{login_url}?{next}".format(
            login_url=settings.LOGIN_URL,
            next=urlencode({REDIRECT_FIELD_NAME: path}),
        )
        request = self.request_factory.get(path)
        request.user = AnonymousUser()
        self.assertRedirects(
            self.middle.process_request(request),
            expected_redirect,
            fetch_redirect_response=False,
        )

    def test_login_successful(self):
        self.request.path_info = "/public/foo"
        self.request.user = User(username="Joe")
        self.assertIsNone(self.middle.process_request(self.request))

    def test_login_not_required(self):
        self.request.path_info = "/public/foo"
        self.request.user = AnonymousUser()
        self.assertIsNone(self.middle.process_request(self.request))


@override_settings(
    URL_GATEKEEPER_LOGIN_EXEMPT_URLS=(r"/public/$", r"/login/$"),
    URL_GATEKEEPER_RESTRICTED_URLS=(
        (r"^/.*x.*/$", "url_gatekeeper.x"),
        (r"^/.*z.*/$", "url_gatekeeper.z"),
        (r"^/staff/*$", "is_staff"),
    ),
    URL_GATEKEEPER_RESTRICTED_URLS_EXCEPTIONS=(r"^.*public.*$",),
)
class TestRequirePermission(URLGateKeeperTestCase):
    def setUp(self):
        self.middle = middle.RequirePermissionMiddleware()
        self.request = HttpRequest()
        user = User(username="Joe")
        user.save()
        self.request.user = user

    def test_view_ok(self):
        def view(request):
            return HttpResponse("OK")

        self.request.path_info = "/login/"
        self.assertIsNone(self.middle.process_view(self.request, view, [], {}))

    def test_view_permitted(self):
        def view(request):
            return HttpResponse("OK")

        self.request.path_info = "/x/"
        self.request.user.has_perm = lambda x: x == "url_gatekeeper.x"
        self.assertIsNone(self.middle.process_view(self.request, view, [], {}))

    def test_exceptions(self):
        def view(request):
            return HttpResponse("OK")

        self.request.path_info = "/x/public/y/"
        self.request.user.has_perm = lambda x: False
        self.assertIsNone(self.middle.process_view(self.request, view, [], {}))

    def test_view_restricted(self):
        def view(request):
            return HttpResponse("OK")

        self.request.path_info = "/x/"
        self.request.user.has_perm = lambda x: x == "url_gatekeeper.y"
        with self.assertRaises(PermissionDenied):
            self.middle.process_view(self.request, view, [], {})

    def test_view_is_staff(self):
        def view(request):
            return HttpResponse("OK")

        self.request.path_info = "/staff/"
        with self.assertRaises(PermissionDenied):
            self.middle.process_view(self.request, view, [], {})
        self.request.user.is_staff = True
        self.assertIsNone(self.middle.process_view(self.request, view, [], {}))

    def test_view_unlisted(self):
        def view(request):
            return HttpResponse("OK")

        self.request.path_info = "/jj/"
        self.request.user.has_perm = lambda x: True
        with self.assertRaises(PermissionDenied):
            self.middle.process_view(self.request, view, [], {})

    def test_view_restricted_twice(self):
        def view(request):
            return HttpResponse("OK")

        self.request.path_info = "/xz/"
        self.request.user.has_perm = lambda x: x == "url_gatekeeper.x"
        with self.assertRaises(PermissionDenied):
            self.middle.process_view(self.request, view, [], {})
        self.request.user.has_perm = lambda x: x == "url_gatekeeper.y"
        with self.assertRaises(PermissionDenied):
            self.middle.process_view(self.request, view, [], {})
        self.request.user.has_perm = lambda x: x in [
            "url_gatekeeper.z",
            "url_gatekeeper.x",
        ]
        self.assertIsNone(self.middle.process_view(self.request, view, [], {}))


@override_settings(URL_GATEKEEPER_TOKEN_REQUIRED_URLS=(r"/image/.*$", r"/img/.*"))
class TestTokenRequired(URLGateKeeperTestCase):
    def setUp(self):
        self.middle = middle.TokenRequiredMiddleware()
        self.request = HttpRequest()
        self.request.path_info = "/image/23/"

    def add_token(self):
        url = add_token("/image/23/")
        params = url.split("?")[1]
        key, value = params.split("=")
        self.request.GET[key] = value

    def test_token_missing(self):
        with self.assertPermissionDenied():
            self.middle.process_request(self.request)

    def test_token_present(self):
        self.add_token()
        self.assertIsNone(self.middle.process_request(self.request))

    def test_token_present_url_wrong(self):
        self.add_token()
        self.request.path_info = "/img/23/"
        with self.assertPermissionDenied():
            self.middle.process_request(self.request)

    def test_token_not_required(self):
        self.add_token()
        self.request.path_info = "/acceptable/23/"
        self.assertIsNone(self.middle.process_request(self.request))
