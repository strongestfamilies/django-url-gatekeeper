from setuptools import setup, find_packages


def long_description(source):
    with open(source, 'r') as f:
        return f.read()


setup(
    name="django-url-gatekeeper-iris",
    version="0.4.1",
    author="Sheepdog",
    author_email="cmeijer@strongestfamilies.com",
    description=("Opt-out permission restrictions by URL patterns"),
    license="BSD",
    keywords="permissions URL",
    url="https://bitbucket.org/connec-iris/django-url-gatekeeper",
    packages=find_packages(exclude=["example"]),
    long_description=long_description("README.md"),
    include_package_data=True,
    install_requires=[
        "six>=1.10.0",
    ],
)
