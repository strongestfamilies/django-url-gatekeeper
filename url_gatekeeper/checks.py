from django.core.checks import Error, register

from .conf import settings


ERROR_CODES = {
    "settings_not_disjoint": "url_gatekeeper.E001",
}


def settings_disjoint_check():
    """
    Check that the attribute and callable whitelists are disjoint.
    """
    errors = []
    if settings.ATTRIBUTE_PERMISSIONS_WHITELIST & settings.CALLABLE_PERMISSIONS_WHITELIST:
        errors.append(
            Error(
                "ATTRIBUTE_PERMISSIONS_WHITELIST and "
                "CALLABLE_PERMISSIONS_WHITELIST must be disjoint sets.",
                hint="Check your configured settings.",
                obj="",
                id=ERROR_CODES["settings_not_disjoint"],
            ),
        )
    return errors


@register()
def settings_check(app_configs, **kwargs):
    """
    Perform a check to ensure that URL Gatekeeper settings pass basic sanity checks.
    """
    errors = []
    errors.extend(settings_disjoint_check())
    return errors
