from .types import (
    ChainedIterablesList,
    ListOfRegexFirstPairs,
    RegexList,
    SetOfPythonIdentifiers,
)


LOGIN_EXEMPT_URLS = RegexList

RESTRICTED_URLS = ListOfRegexFirstPairs

RESTRICTED_URLS_EXCEPTIONS = RegexList

TOKEN_REQUIRED_URLS = RegexList

CACHE_NAME = "shared"

DEFAULT_EXPIRY = 10


class ATTRIBUTE_PERMISSIONS_WHITELIST(SetOfPythonIdentifiers):
    default = ["is_staff", "is_authenticated", "is_anonymous"]


class CALLABLE_PERMISSIONS_WHITELIST(SetOfPythonIdentifiers):
    default = []


class ALL_URLS_EXCEPTIONS(ChainedIterablesList):
    settings = [
        "RESTRICTED_URLS_EXCEPTIONS",
        "LOGIN_EXEMPT_URLS",
    ]
