django-url-gatekeeper
=====================

django-url-gatekeeper is an app with opt-out permissions based on URL
only.   This has the disadvantage of being less pythonic, but has the
advantages of default permission being "nobody" and a central list of
permissions.

Additionally, django-url-gatekeeper supplies an API for supplying a
one-use TOKEN to a URL pattern, making the view temporarily
admissible.

Usage
-----

See examples/settings.py for sample configurations.  In particular, in
settings.py you will need to add to INSTALLED_APPS:

    'url_gatekeeper',

and add any or all of the following `MIDDLEWARE_CLASSES`:

    'url_gatekeeper.middleware.LoginRequiredMiddleware',
    'url_gatekeeper.middleware.TokenRequiredMiddleware',
    'url_gatekeeper.middleware.RequirePermissionMiddleware',

If you choose to use `TokenRequiredMiddleware`, you will need to specify
a 'shared' cache which is shared across all threads.  For example:

    CACHES = {
        'default': {
            'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        },
        'shared': {
            'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
            'LOCATION': '/tmp/django_cache',
        }
    }

Lastly, configure the settings.  Here is a sample:

    LOGIN_URL = '/login/'
    LOGOUT_URL = '/logout/'
    URL_GATEKEEPER_LOGIN_EXEMPT_URLS = (
        r'^/public/',
    )
    URL_GATEKEEPER_TOKEN_REQUIRED_URLS = (
        r'^/report_on_last_view/',
    )
    URL_GATEKEEPER_RESTRICTED_URLS = (
        (r'^/private/', 'is_staff'),
        (r'^/semiprivate/', 'can_view_semiprivate'),
        (r'^/login/', None),
        (r'^/logout/', None),
     )

    URL_GATEKEEPER_RESTRICTED_URLS_EXCEPTIONS = (
        r'^/semiprivate/public/',
    )


Meaning of URL_GATEKEEPER_RESTRICTED_URLS
-------------------------------------------
Middleware component that wraps the permission_check decorator around
views for matching URL patterns. To use, add the class to
`MIDDLEWARE_CLASSES` and define `URL_GATEKEEPER_RESTRICTED_URLS` in your settings.py.

- All matching restricted urls must be met.

- At least one restricted urls must match.

- To exempt a URL from requiring permissions, use None.

Each permission string can either be a permission, a
boolean property of a user, or a callable attribute of a user that returns a boolean.

If it is a boolean property of the user, then it must be whitelisted in
`URL_GATEKEEPER_ATTRIBUTE_PERMISSIONS_WHITELIST`.  By default, this is set to
`['is_staff', 'is_superuser']`

If it is a callable attribute of the user, then it must be whitelisted in
`URL_GATEKEEPER_CALLABLE_PERMISSIONS_WHITELIST`.  By default, this is set to
`['is_authenticated']`


URL_GATEKEEPER_RESTRICTED_URLS_EXCEPTIONS
-------------------------------------------
This is simply a list of patterns which are not subject to
`URL_GATEKEEPER_RESTRICTED_URLS`.  This should only be used rarely.  The recommended
way to declare that a URL does not require permissions is to use a
permission specification of None in `RESTRICTED_URLS`.   There are two
reasons why this feature is supplied:

1.  For backwards compatability

2.  To simplify what would otherwise be a complex set of patterns to
indicate that most of a set of URLs require the same permissions, but that
there are a few exceptions which are open.


Using TOKENS
------------
To use tokens in URL which cause the URL to expire, two steps are
required:  Add the url to `URL_GATEKEEPER_TOKEN_REQUIRED_URLS`, and then be sure
that the token is added as a GET parameter to the link using, say:

    from url_gatekeeper import add_token
    return add_token('/temporary/?user=23', expiry=30) # (expiry defaults to settings.URL_GATEKEEPER_DEFAULT_EXPIRY seconds, default 10)

The mechanism is useful for both "locking" the GET parameters at URL
generation as well as for presenting dynamic data which the user
should only view as it was most recently prepared.

The token is stored in a cache named `URL_GATEKEEPER_CACHE_NAME`, default `"shared"`.
