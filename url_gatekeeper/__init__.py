# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

from six.moves import range
from six.moves import urllib


import random
import string
try:
    from django.core.cache import caches

    def get_cache(cache_name):
        return caches[cache_name]


except ImportError:
    from django.core.cache import get_cache

from .conf import settings


def random_string(length=50, chars=None):
    chars = chars if chars else string.ascii_lowercase
    return "".join(random.choice(chars) for x in range(length))


def add_query_param(url, name, value):
    """
    Add the query param name=value to the url string and return a new url string.

    >>> add_query_param('http://www.nyt.com/?last=Smith#bar', 'name', 'John')
    'http://www.nyt.com/?last=Smith&name=John#bar'
    """
    o = urllib.parse.urlparse(url)
    qs = urllib.parse.parse_qs(o.query)
    qs[name] = [value]
    o2 = urllib.parse.ParseResult(
        o.scheme,
        o.netloc,
        o.path,
        o.params,
        urllib.parse.urlencode(qs, doseq=True),
        o.fragment,
    )
    return o2.geturl()


def add_token(url, expiry=None):
    if expiry is None:
        expiry = settings.DEFAULT_EXPIRY
    token = random_string()
    get_cache(settings.CACHE_NAME).set(token, url, expiry)
    return add_query_param(url, "url_token", token)


def check_token(token, url):
    url2 = get_cache(settings.CACHE_NAME).get(token)
    return token and url == urllib.parse.unquote(url2)
