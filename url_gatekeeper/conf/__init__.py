from django.conf import settings as django_settings

from . import defaults
from .base import AppSettings


settings = AppSettings(
    django_settings=django_settings,
    defaults=defaults,
    app_label="url_gatekeeper",
    used_global_settings=[
        # TODO: these should be made app settings
        "LOGIN_URL",
        "LOGOUT_URL",
    ],
)
