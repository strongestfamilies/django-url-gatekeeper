# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

from six.moves.urllib.parse import unquote


import logging

from django.core.exceptions import PermissionDenied
from django.core.cache import caches
from django.http import HttpResponseRedirect
from django.utils.http import urlencode
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.utils.deprecation import MiddlewareMixin

try:
    from django.utils.deprecation import CallableTrue
except ImportError:
    CallableTrue = True

from .conf import settings
from .utils import gather_permissions


log = logging.getLogger(__name__)


def _checkperm(
    user,
    perm,
    attributes_list=settings.ATTRIBUTE_PERMISSIONS_WHITELIST,
    callable_list=settings.CALLABLE_PERMISSIONS_WHITELIST,
):
    if perm in attributes_list:
        # Handles, say, is_staff; insist on True to avoid 'truthy' false positives
        val = getattr(user, perm, None)
        return val is True or val is CallableTrue
    if perm in callable_list:
        return getattr(user, perm, None)() is True
    return user.has_perm(perm)


def permissions_required_with_403(perms, request):
    """
    Works similar to django.contrib.auth.decorators.permission_required but
    (a) raise permission denied instead of redirecting to login, and
    (b) requires all permissions listed
    """
    for perm in perms:
        if not _checkperm(request.user, perm):
            raise PermissionDenied("Requires permission %s" % perm)
    return None


class LoginRequiredMiddleware(MiddlewareMixin):
    """
    Middleware that requires a user to be authenticated to view any page other
    than those listed in settings.LOGIN_EXEMPT_URLS.

    Requires authentication middleware and template context processors to be
    loaded. You'll get an error if they aren't.

    From http://djangosnippets.org/snippets/1179/
    """

    exempt_urls = settings.LOGIN_EXEMPT_URLS

    def process_request(self, request, redirect_field_name=REDIRECT_FIELD_NAME):
        if not hasattr(request, "user"):
            raise AssertionError(
                "The Login Required middleware requires authentication middleware "
                "to be installed. Edit your MIDDLEWARE_CLASSES setting to insert "
                "'django.contrib.auth.middlware.AuthenticationMiddleware'. If "
                "that doesn't work, ensure your TEMPLATE_CONTEXT_PROCESSORS "
                "setting includes 'django.core.context_processors.auth'.",
            )

        if request.user.is_authenticated or any(
            m.match(request.path_info) for m in self.exempt_urls
        ):
            return
        elif request.path_info == settings.LOGOUT_URL:
            return HttpResponseRedirect(self.login_url)
        else:
            return HttpResponseRedirect(redirect_to="{login_url}?{next}".format(
                login_url=settings.LOGIN_URL,
                next=urlencode({redirect_field_name: request.get_full_path()}),
            ))


class RequirePermissionMiddleware(MiddlewareMixin):
    """
    Middleware component that wraps the permission_check decorator around
    views for matching URL patterns. To use, add the class to
    MIDDLEWARE_CLASSES and define RESTRICTED_URLS and
    RESTRICTED_URLS_EXCEPTIONS in your settings.py.

    URLs listed in RESTRICTED_URLS_EXCEPTIONS are exempt for checks by
    this middleware.  Otherwise,
       - All matching RESTRICTED_URLS must be met.
       - At least one RESTIRCTED_URLS must match.
    For example:

    RESTRICTED_URLS = (
                          (r'^/topsecet/$', 'auth.access_topsecet'),
                          (r'^/admin$', 'is_staff'),
                          (r'^/.*$', 'is_authenticated'),
                      )
    RESTRICTED_URLS_EXCEPTIONS = (
                          r'^/login/$',
                          r'^/logout/$',
                      )

    Adapted from http://djangosnippets.org/snippets/1219/
    """

    restricted = settings.RESTRICTED_URLS
    exceptions = settings.ALL_URLS_EXCEPTIONS

    def process_view(self, request, view_func, view_args, view_kwargs):
        # An exception match should immediately return None
        for path in self.exceptions:
            if path.match(request.path_info):
                return None

        required_permissions = tuple(gather_permissions(request.path_info, self.restricted))
        if required_permissions:
            required_permissions = (p for p in required_permissions if p is not None)
            return permissions_required_with_403(required_permissions, request)
        else:
            msg = (
                "No user can view url %s. "
                "There is no permission setting in RESTRICTED_URLS."
            ) % request.path_info
            log.warning(msg)
            raise PermissionDenied(msg)


def check_token(token, url):
    url2 = caches[settings.CACHE_NAME].get(token)
    return token and url == unquote(url2)


class TokenRequiredMiddleware(MiddlewareMixin):
    token_required = settings.TOKEN_REQUIRED_URLS

    def process_request(self, request):
        path_info = request.path_info
        for m in self.token_required:
            if m.match(path_info):
                token = request.GET.get("url_token")
                if token is None:
                    log.warning(
                        "Token required url %s submitted without a token." % path_info
                    )
                    raise PermissionDenied(
                        "View cannot be bookmarked. "
                        "This page requires an access token, but none was found.",
                    )
                log.debug("Checking token %s against url %s" % (token, path_info))
                try:
                    if not (check_token(token, path_info)):
                        log.warning(
                            "Token required url %s submitted without mismatched token %s."
                            % (path_info, token),
                        )
                        raise PermissionDenied(
                            "View cannot be bookmarked. This page requires an access token, "
                            "but the access token doesn't match the url.",
                        )
                except AttributeError:
                    log.warning(
                        "Token required url %s submitted with stale token %s."
                        % (path_info, token),
                    )
                    raise PermissionDenied(
                        "View cannot be bookmarked. This page requires an access token, "
                        "but the access token is stale.",
                    )
                break
