# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function

from six.moves import reload_module as reload


from contextlib import contextmanager

from django.test import TestCase
from django.core.exceptions import PermissionDenied

import url_gatekeeper.middleware as middle



def refresh_url_gatekeeper_settings():
    reload(middle)


class URLGateKeeperTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(URLGateKeeperTestCase, cls).setUpClass()
        refresh_url_gatekeeper_settings()

    @classmethod
    def tearDownClass(cls):
        super(URLGateKeeperTestCase, cls).tearDownClass()
        refresh_url_gatekeeper_settings()

    @contextmanager
    def assertPermissionDenied(self):
        with self.assertRaises(PermissionDenied):
            yield
