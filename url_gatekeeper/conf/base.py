# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function


import itertools

from django.conf import Settings


class Setting(object):
    default = None
    derived = False

    @classmethod
    def value(cls):
        return cls.get_value(cls.default)

    @classmethod
    def compute(cls, value):
        if Setting.is_implemented_by(value):
            return value.value()
        return cls.get_value(value)

    @classmethod
    def get_value(cls, value):
        return value

    @staticmethod
    def is_implemented_by(obj):
        return (
            isinstance(obj, Setting)
            or (isinstance(obj, type) and issubclass(obj, Setting))
        )


class DerivedSetting(Setting):
    settings = ()
    derived = True
    writeback = False
    constant = True

    @classmethod
    def compute(cls, settings_obj):
        return cls.value_from_settings(*(
            getattr(settings_obj, name) for name in cls.settings
        ))

    @classmethod
    def value_from_settings(cls, *values):
        raise NotImplementedError()


# Default used in a getattr call below
def setting_not_overridden(setting):
    return False


class AppSettings(Settings):
    """
    Settings subclass that writes back defaults to the main django_settings
    and exposes a whitelist of those settings to the app.

    App Settings, provided by the defaults arg, are prefixed with the upper-cased
    app_label where all "."s are replaced by "_"  when written back to the main django_settings.
    If no app_label is provided, no prefixing will happen.
    """
    _explicit_settings = set()
    _all_settings = set()

    def __init__(
        self,
        django_settings,
        defaults,
        app_label=None,
        used_global_settings=(),
    ):
        # For compiling settings to a usable value
        self._setting_processors = {}
        self._calculated_settings = {}
        self.SETTINGS_MODULE = getattr(django_settings, 'SETTINGS_MODULE',
                                       None)
        self.app_label = app_label
        self._unprefix = "%s_" % (app_label and app_label.upper().replace(".", "_"))
        self._prefix = self._unprefix + '%s'
        self.django_settings = django_settings
        self.used_global_settings = set(used_global_settings)

        # Record the original unprocessed settings here
        # so we can check if they changed.
        self._explicit_settings = {}
        self._declared_settings = {
            name for name in dir(defaults) if name.isupper()
        }
        self._all_settings = set()

        self._update_from_settings(
            defaults,
            explicit=False,
            writeback_settings=django_settings,
        )
        self._update_from_settings(
            django_settings,
            explicit=True,
            unprefix=self._unprefix,
            whitelist=set(itertools.chain(
                used_global_settings,
                self._declared_settings,
            )),
        )
        self._initialized = True

    def prefixer(self, name):
        if name in self._declared_settings:
            return self._prefix % name
        return name

    def add_explicit_setting(self, name, original_value):
        # Store the original value in a WeakValueDictionary
        # so we can avoid expensive recomputation unless it
        # changes.

        if (
            name in self._declared_settings
            or name in self.used_global_settings
        ):
            computed_value, changed = self.maybe_recompute_value(name, original_value)
            if changed:
                self._explicit_settings[name] = original_value
            return computed_value, changed
        else:
            raise ValueError(
                "Setting %s is not used in %s."
                % (name, self.app_label or "this app"),
            )

    def maybe_recompute_value(self, name, val):
        current_value = self._explicit_settings.get(name)
        if current_value is val:
            # We've already computed it; return our version
            return (object.__getattribute__(self, name), False)
        processor = self._setting_processors.get(name)
        if processor:
            return (processor(val), True)
        return (val, True)

    def __setattr__(self, name, val):
        if getattr(self, '_initialized', False):
            if name.startswith(self._unprefix):
                name = name[len(self._unprefix):]
            new_val, changed = self.add_explicit_setting(name, val)
            if changed:
                # We store the original value on the main settings
                setattr(self.django_settings, name, val)
            val = new_val
        return super(AppSettings, self).__setattr__(name, val)

    def __getattribute__(self, name):
        # First we check django settings in case something changed
        if name.isupper() and name in self._all_settings:
            prefixed_name = self.prefixer(name)
            calculated_settings = self._calculated_settings
            setting = calculated_settings.get(name)

            # Try to get the setting from Django
            # If it isn't a constant setting
            if not setting or not setting.constant:
                try:
                    val = getattr(self.django_settings, prefixed_name)
                except AttributeError:
                    pass
                else:
                    new_val, changed = self.add_explicit_setting(name, val)
                    if changed:
                        object.__setattr__(self, name, new_val)
                        return new_val
            elif setting:
                # NOTE: if we gathered up the reverse dependencies
                # in a dict, we could avoid recomputing the value
                # if a dependency didn't change.
                value = setting.compute(self)
                if setting.writeback:
                    # Oddity: derived settings will be calculated in django settings
                    # if they're written back.
                    setattr(self.django_settings, prefixed_name, value)
                return value
        return object.__getattribute__(self, name)

    def _validate(self, setting, whitelist=None):
        if whitelist is None or setting in whitelist:
            return True
        return False

    def _update_from_settings(
        self,
        settings,
        explicit=True,
        whitelist=None,
        writeback_settings=None,
        unprefix='',
        force_writeback=False,
    ):
        setting_processors = self._setting_processors
        calculated_settings = self._calculated_settings
        all_settings = self._all_settings
        explicit_settings = self._explicit_settings
        for setting in dir(settings):
            if not setting.isupper():
                continue
            setting_to_load = setting
            processor = None
            if explicit:  # This setting came from a user (prefixed)
                if unprefix and setting_to_load.startswith(unprefix):
                    setting_to_load = setting_to_load[len(unprefix):]
                    processor = setting_processors.get(setting_to_load)
                elif not self._validate(setting_to_load, whitelist=whitelist):
                    continue

            setting_val = getattr(settings, setting)
            orig_val = setting_val
            if (
                not explicit  # this setting is a default (unprefixed)
                and Setting.is_implemented_by(setting_val)
            ):
                # Calculated settings must be delayed until we're
                # fully populated.  There isn't yet support for
                # dependencies between calculated settings.
                if setting_val.derived:
                    calculated_settings[setting_to_load] = setting_val
                    all_settings.add(setting_to_load)
                    continue
                setting_processors[setting_to_load] = setting_val.compute
                setting_val = setting_val.compute(setting_val)
            elif processor:
                setting_val = processor(setting_val)
                # NOTE: We don't write back the computed value yet
                # Maybe we should? But we'd need some way to tell
                # in __getattribute__ that the value had already been
                # computed...  not an easy problem.  We could just add
                # another function to the Setting class to test if a value
                # had already been computed?

            setattr(
                self,
                setting_to_load,
                setting_val,
            )

            all_settings.add(setting_to_load)
            if explicit:
                # Record the original value so we can check if it
                # changed later.
                explicit_settings[setting_to_load] = orig_val

            if writeback_settings is not None:
                new_setting = self.prefixer(setting)
                if (
                    force_writeback
                    # Unittests: if it's overridden,
                    # don't bother to write back (not sure about the logic here)
                    or not getattr(writeback_settings, "is_overridden", setting_not_overridden)(
                        new_setting,
                    ) and not hasattr(writeback_settings, new_setting)
                ):
                    # Currently we record to original value back to
                    # settings (not the computed value).
                    setattr(
                        writeback_settings,
                        new_setting,
                        orig_val,
                    )
