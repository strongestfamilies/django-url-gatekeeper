

def gather_permissions(path_info, config):
    for perm in (perm for url, perm in config if url.match(path_info)):
        yield perm
