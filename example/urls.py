# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function


from django.conf.urls import include, url
from django.contrib.auth import views as auth_views
from django.contrib import admin

from example import views

admin.autodiscover()

urlpatterns = [
    url(r"^$", views.home),
    url(r"^\(|public|private|semiprivate\)/$", views.home),
    url(r"^admin/", include(admin.site.urls)),
    url(r"^login/$", auth_views.LoginView.as_view(template_name = "registration/login.html"), name="login"),
    url(r"^logout/$", auth_views.LogoutView.as_view(), name="logout"),
]
