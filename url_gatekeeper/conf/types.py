from six import string_types


import re
import string
from itertools import chain

from .base import DerivedSetting, Setting


# Used by identifier_or_error
_EXPECTED_FIRST = set("_" + string.ascii_lowercase + string.ascii_uppercase)
_EXPECTED_OTHER = _EXPECTED_FIRST | set(string.digits)


def identifier_or_error(ident):
    """Determines if string is valid Python identifier."""
    if not isinstance(ident, string_types):
        raise TypeError("expected str, but got {!r}".format(type(ident)))

    checks = (
        lambda i: not i,
        lambda i: i[0] not in _EXPECTED_FIRST,
        lambda i: any(ch not in _EXPECTED_OTHER for ch in i[1:]),
    )
    for check in checks:
        if check(ident):
            raise ValueError("%s is not a valid identifier." % ident)
    return ident


class RegexList(Setting):
    default = ()

    @classmethod
    def get_value(cls, value):
        return tuple(re.compile(r) for r in value)


class ListOfRegexFirstPairs(Setting):
    default = ()

    @classmethod
    def get_value(cls, value):
        return tuple((re.compile(r), _) for r, _ in value)


class SetOfPythonIdentifiers(Setting):
    default = frozenset()

    @classmethod
    def get_value(cls, value):
        return frozenset(identifier_or_error(ident) for ident in value)


class ChainedIterablesList(DerivedSetting):
    @classmethod
    def value_from_settings(cls, *values):
        return tuple(chain(*values))
